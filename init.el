;;; init.el -*- lexical-binding: t; -*-

;; Copy this file to ~/.doom.d/init.el or ~/.config/doom/init.el ('doom
;; quickstart' will do this for you). The `doom!' block below controls what
;; modules are enabled and in what order they will be loaded. Remember to run
;; 'doom refresh' after modifying it.
;;
;; More information about these modules (and what flags they support) can be
;; found in modules/README.org.

(doom! :completion
       (company +auto)
       helm
       ;;ido
       ivy

       :ui
       deft
       doom
       doom-dashboard
       ;;doom-quit
       ;;fill-column
       hl-todo
       indent-guides
       modeline
       ;;nav-flash
       ;;neotree
       ophints
       (popup +all +defaults)
       ;;pretty-code
       ;;tabbar
       treemacs
       ;;unicode
       vc-gutter
       vi-tilde-fringe
       window-select
       workspaces

       :editor
       (evil +everywhere)
       file-templates
       fold
       (format +onsave)
       lispy
       multiple-cursors
       ;;parinfer
       rotate-text
       snippets

       :emacs
       dired
       electric
       undo
       vc

       :term
       eshell
       ;;term
       vterm

       :checkers
       syntax
       spell
       grammar

       :tools
       ;;ansible
       ;;debugger
       direnv
       ;;docker
       editorconfig
       ;;ein
       eval
       (lookup +docsets)
       lsp
       ;;macos
       magit
       make
       pass
       pdf
       ;;prodigy
       ;;rgb
       ;;terraform
       ;;tmux
       upload
       ;;wakatime

       :lang
       agda
       ;;assembly
       ;;cc
       clojure
       ;;common-lisp
       ;;coq
       ;;crystal
       ;;csharp
       data
       ;;erlang
       ;;elixir
       ;;elm
       emacs-lisp
       ;;ess
       (go +lsp)
       (haskell +dante)
       ;;hy
       ;;idris
       ;;(java +meghanada)
       ;;javascript
       ;;julia
       ;;kotlin
       latex
       ;;ledger
       lua
       markdown
       ;;nim
       nix
       ocaml
       (org +dragndrop +ipython +pandoc +gnuplot +present)
       ;;perl
       ;;php
       ;;plantuml
       ;;purescript
       python
       ;;qt
       racket
       ;;rest
       ;;ruby
       rust
       ;;scala
       sh
       ;;solidity
       ;;swift
       ;;terra
       web
       ;;vala

       :email
       ;;(mu4e +gmail)
       ;;notmuch
       ;;(wanderlust +gmail)

       ;; Applications are complex and opinionated modules that transform Emacs
       ;; toward a specific purpose. They may have additional dependencies and
       ;; should be loaded late.
       :app
       ;;calendar
       irc
       ;;(rss +org)
       ;;twitter

       :config
       ;; For literate config users. This will tangle+compile a config.org
       ;; literate config in your `doom-private-dir' whenever it changes.
       ;;literate

       ;; The default module sets reasonable defaults for Emacs. It also
       ;; provides a Spacemacs-inspired keybinding scheme and a smartparens
       ;; config. Use it as a reference for your own modules.
       (default +bindings +smartparens))
