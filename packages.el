;; -*- no-byte-compile: t; -*-
;;; ~/.doom.d/packages.el

(package! agda-input
  :recipe
  (:host github :repo "agda/agda" :branch "release-2.6.0.1"
         :files ("src/data/emacs-mode/agda-input.el")))

(package! agda2-mode
  :recipe
  (:host github :repo "agda/agda" :branch "release-2.6.0.1"
         :files
         ("src/data/emacs-mode/*.el"
          (:exclude "agda-input.el"))))
