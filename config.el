;;; ~/.doom.d/config.el -*- lexical-binding: t; -*-

(setq user-full-name "Samodya Abeysiriwardane"
      user-mail-address "hi@sransara.com")

(setq doom-font (font-spec :family "Hack" :size 12))

(setq custom-file (expand-file-name "custom.el" doom-private-dir))
(load custom-file)

;; Agda
(add-to-list 'auto-mode-alist '("\\.lagda.md\\'" . agda2-mode))

;; Perl
(after! perl-mode
  (set-company-backend! 'perl-mode '(company-dabbrev-code))
  (setq flycheck-perlcritic-severity 1
        flycheck-perlcritic-theme "pbp"))
